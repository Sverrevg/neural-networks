﻿using checkerboard2.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace checkerboard2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ConvertImage();
            CreateNetwork();
        }

        private Bitmap bmp; //bitmap for image
        List<int> values = new List<int>(); //list to store values

        private void ConvertImage()
        {
            bmp = new Bitmap("C:/Users/sverr/Desktop/AI/checker1.jpg"); //get image

            for (int y = 0; y < 2; y++)
            {
                for (int x = 0; x < 2; x++)
                {
                    int col = 0;
                    Color now_color = bmp.GetPixel(x, y);

                    if (now_color.ToArgb() == Color.Black.ToArgb()) //Compare Pixel's Color ARGB property with ARGB property 
                    {
                        col = -1;
                    }
                    else if (now_color.ToArgb() == Color.White.ToArgb()) //check for white
                    {
                        col = 1;
                    }
                    values.Add(col);
                }
            }
        }

        private void CreateNetwork()
        {
            NetworkModel network = new NetworkModel(values);

            //visualize output
            if (network.layer2[0].output == 1)
            {
                outputText.Text = "I think it's this one:";
                pixel0.BackColor = Color.Black;
                pixel1.BackColor = Color.White;
                pixel2.BackColor = Color.White;
                pixel3.BackColor = Color.Black;
            }
            else if (network.layer2[1].output == 1)
            {
                outputText.Text = "I think it's this one:";
                pixel0.BackColor = Color.White;
                pixel1.BackColor = Color.Black;
                pixel2.BackColor = Color.Black;
                pixel3.BackColor = Color.White;
            }
            else
            {
                outputText.Text = "I can't find a checkerbox here...";
            }
        }
    }
}
