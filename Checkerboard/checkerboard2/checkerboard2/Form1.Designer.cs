﻿namespace checkerboard2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.outputText = new System.Windows.Forms.Label();
            this.pixel0 = new System.Windows.Forms.Panel();
            this.pixel1 = new System.Windows.Forms.Panel();
            this.pixel2 = new System.Windows.Forms.Panel();
            this.pixel3 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // outputText
            // 
            this.outputText.AutoSize = true;
            this.outputText.Location = new System.Drawing.Point(13, 13);
            this.outputText.Name = "outputText";
            this.outputText.Size = new System.Drawing.Size(274, 17);
            this.outputText.TabIndex = 0;
            this.outputText.Text = "I think the image contains this checkerbox:";
            // 
            // pixel0
            // 
            this.pixel0.Location = new System.Drawing.Point(16, 43);
            this.pixel0.Name = "pixel0";
            this.pixel0.Size = new System.Drawing.Size(100, 100);
            this.pixel0.TabIndex = 1;
            // 
            // pixel1
            // 
            this.pixel1.Location = new System.Drawing.Point(122, 43);
            this.pixel1.Name = "pixel1";
            this.pixel1.Size = new System.Drawing.Size(100, 100);
            this.pixel1.TabIndex = 2;
            // 
            // pixel2
            // 
            this.pixel2.Location = new System.Drawing.Point(16, 149);
            this.pixel2.Name = "pixel2";
            this.pixel2.Size = new System.Drawing.Size(100, 100);
            this.pixel2.TabIndex = 3;
            // 
            // pixel3
            // 
            this.pixel3.Location = new System.Drawing.Point(122, 149);
            this.pixel3.Name = "pixel3";
            this.pixel3.Size = new System.Drawing.Size(100, 100);
            this.pixel3.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pixel3);
            this.Controls.Add(this.pixel2);
            this.Controls.Add(this.pixel1);
            this.Controls.Add(this.pixel0);
            this.Controls.Add(this.outputText);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label outputText;
        private System.Windows.Forms.Panel pixel0;
        private System.Windows.Forms.Panel pixel1;
        private System.Windows.Forms.Panel pixel2;
        private System.Windows.Forms.Panel pixel3;
    }
}

