﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace checkerboard2.Classes
{
    class NetworkModel
    {
        public NetworkModel(List<int> values)
        {
            val = values;
            Layer1();
            Layer2();
        }

        public List<Neuron> layer1 = new List<Neuron>(); //list to save layer1 pulses
        public List<Neuron_L2> layer2 = new List<Neuron_L2>(); //list to save layer2 pulses
        private List<int> val = new List<int>(); //list to save input pulses
        private List<int> toCheck = new List<int>(); //list to save what inputs the neurons have to proces

        private int valToGet;
        private int input1, input2;

        //hardcoded as of now
        private void Layer1() //make a for loop out of this method? need a cleaner/more efficient and adaptive way to create a network of neurons
        {
            toCheck.Add(-1); //add two empty values to checklist first
            toCheck.Add(-1);

            //first neuron checks for sqr 0 and 3 to be white
            toCheck[0] = 0;
            toCheck[1] = 3;
            valToGet = 1;
            Neuron neuron0 = new Neuron(val, toCheck, valToGet);
            layer1.Add(neuron0);

            //second neuron checks for sqr 0 and 3 to be black
            toCheck[0] = 0;
            toCheck[1] = 3;
            valToGet = -1;
            Neuron neuron1 = new Neuron(val, toCheck, valToGet);
            layer1.Add(neuron1);

            //third neuron checks for sqr 1 and 2 to be white
            toCheck[0] = 1;
            toCheck[1] = 2;
            valToGet = 1;
            Neuron neuron2 = new Neuron(val, toCheck, valToGet);
            layer1.Add(neuron2);

            //fourth neuron checks for sqr 1 and 2 to be black
            toCheck[0] = 1;
            toCheck[1] = 2;
            valToGet = -1;
            Neuron neuron3 = new Neuron(val, toCheck, valToGet);
            layer1.Add(neuron3);
        }

        private void Layer2() //same for this one?
        {
            //first neuron checks for bwbw combination
            input1 = layer1[1].outputPulse;
            input2 = layer1[2].outputPulse;
            Neuron_L2 neuron4 = new Neuron_L2(input1, input2);
            layer2.Add(neuron4);

            //second neuron checks for wbwb combination
            input1 = layer1[0].outputPulse;
            input2 = layer1[3].outputPulse;
            Neuron_L2 neuron5 = new Neuron_L2(input1, input2);
            layer2.Add(neuron5);
        }
    }
}
