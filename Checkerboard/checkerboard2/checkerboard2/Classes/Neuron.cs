﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace checkerboard2.Classes
{
    class Neuron
    {
        public int input, input2, outputPulse;
        private int bias, valToGet;
        private List<int> val = new List<int>();
        private List<int> toCheck = new List<int>();

        public Neuron(List<int> values, List<int> check, int valTrue)
        {
            toCheck = check;
            val = values;
            valToGet = valTrue;
            bias = -1;

            if (val[toCheck[0]] == valToGet)
            {
                input = 1;
            }
            else
            {
                input = -1;
            }

            if (val[toCheck[1]] == valToGet)
            {
                input2 = 1;
            }
            else
            {
                input2 = -1;
            }

            outputPulse = input + input2 + bias;

            //curve output
            if (outputPulse < 1)
            {
                outputPulse = -1;
            }
        }

        public override string ToString()
        {
            return outputPulse.ToString();
        }
    }
}
