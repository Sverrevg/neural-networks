﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace checkerboard2.Classes
{
    class Neuron_L2
    {
        public int input1, input2;
        public int output, bias;

        public Neuron_L2(int i1, int i2)
        {
            input1 = i1;
            input2 = i2;
            bias = -1;
            output = input1 + input2 + bias;

            //curve output
            if (output < 1)
            {
                output = -1;
            }
        }

        public override string ToString()
        {
            return output.ToString();
        }
    }
}
