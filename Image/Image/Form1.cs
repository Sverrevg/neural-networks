﻿using Image.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Image
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ConvertImage();
            Retina(); //first layer: retina: see information
            V1(); //second layer: Visual 1: check for lines (horizontal and vertical)
            V2(); //third layer: Visual 2: check for corners
            V4(); //fourth layer: Visual 4: make connections between corners and fire
            final(); //last layer: Processing: guess answer based on fired neurons in V3
        }

        private Bitmap bmp; //bitmap for image
        Retina[,] layerR = new Retina[5, 5]; //array to save retina neurons
        List<Retina> R = new List<Retina>(); //list to save retina neurons
        V1[,] layerV1 = new V1[5,5]; //array to save V1 neurons
        V1[,] layerV1_2 = new V1[5,5]; //second array to save V1 neurons
        List<V1> Visual1 = new List<V1>(); //list to save V1 neurons

        private void ConvertImage()
        {
            bmp = new Bitmap("C:/Users/sverr/Desktop/AI/drawing.jpg"); //get image
        }

        private void Retina() //first layer
        {
            for (int i = 0; i < 5; i++) //repeat for every chunk in y axis
            {
                for (int z = 0; z < 5; z++) //repeat for every chunk in x axis
                {
                    int filled = 0;
                    for (int y = i * 100; y < 100 * i + 100; y++)
                    {
                        for (int x = z * 100; x < 100 * z + 100; x++)
                        {
                            Color now_color = bmp.GetPixel(x, y);
                            
                            if (now_color.ToArgb() == Color.Black.ToArgb()) //Compare Pixel's Color ARGB property with ARGB property 
                            {
                                filled++;
                            }
                        }
                    }
                    Retina rCell = new Retina() { filledAmount = filled};

                    if (filled >= 1500)
                    {
                        rCell.fire = true;
                    }

                    layerR[i, z] = rCell; //save to array
                    R.Add(rCell);
                }
            }
            retinaList.DataSource = R;
        }

        private void V1() //second layer: check for lines (horizontal and vertical)
        {
            //check for horizontal line. Each V1 checks three R neurons
            for (int y = 0; y < 5; y++)
            {
                int streak = 0;
                bool fired = false;
                for (int x = 0; x < 5; x++)
                {
                    if (layerR[y, x].fire) //check if retina neuron fired
                    {
                        streak++;
                        fired = true;
                    }
                    else if (layerR[y, x].fire == false) //if not fired, reset
                    {
                        streak = 0;
                        fired = false;
                    }

                    if (streak == 3 && fired) //when three in a row, fire V1
                    {
                        streak = 0;
                        V1 v1cell = new V1() { fire = true };
                        layerV1[y,x] = v1cell;
                    }

                    else if (fired == false)
                    {
                        V1 v1cell = new V1() { fire = false };
                        layerV1[y, x] = v1cell;
                    }
                }
            }

            //check for vertical line
            for (int x = 0; x < 5; x++)
            {
                int streak = 0;
                bool fired = false;
                for (int y = 0; y < 5; y++)
                {
                    if (layerR[y, x].fire) //check if retina neuron fired
                    {
                        streak++;
                        fired = true;
                    }
                    else if (layerR[y, x].fire == false) //if not fired, reset
                    {
                        streak = 0;
                        fired = false;
                    }

                    if (streak == 3 && fired) //when three in a row, fire V1
                    {
                        V1 v1cell = new V1() { fire = true };
                        layerV1_2[y, x] = v1cell;
                        streak = 0;
                    }

                    else if (fired == false)
                    {
                        V1 v1cell = new V1() { fire = false };
                        layerV1_2[y, x] = v1cell;
                    }
                }
            }
        }


        private void V2() //third layer: check for corners
        {
            //if a horizontal and vertical line are connected, fire neuron.
            //compare two arrays
            //check neighbouring indexes (+/-1) for fired neurons
            //if true, fire for corner
        }

        private void V4() //fourth layer: Visual 4: make connections between corners and fire
        {
            
        }

        private void final() //last layer: Processing: guess answer based on fired neurons in V3
        {
            
        }
    }
}
