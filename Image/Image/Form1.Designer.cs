﻿namespace Image
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.retinaLabel = new System.Windows.Forms.Label();
            this.retinaList = new System.Windows.Forms.ListBox();
            this.v1List = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // retinaLabel
            // 
            this.retinaLabel.AutoSize = true;
            this.retinaLabel.Location = new System.Drawing.Point(13, 13);
            this.retinaLabel.Name = "retinaLabel";
            this.retinaLabel.Size = new System.Drawing.Size(53, 17);
            this.retinaLabel.TabIndex = 0;
            this.retinaLabel.Text = "Retina:";
            // 
            // retinaList
            // 
            this.retinaList.FormattingEnabled = true;
            this.retinaList.ItemHeight = 16;
            this.retinaList.Location = new System.Drawing.Point(16, 33);
            this.retinaList.Name = "retinaList";
            this.retinaList.Size = new System.Drawing.Size(180, 436);
            this.retinaList.TabIndex = 1;
            // 
            // v1List
            // 
            this.v1List.FormattingEnabled = true;
            this.v1List.ItemHeight = 16;
            this.v1List.Location = new System.Drawing.Point(202, 33);
            this.v1List.Name = "v1List";
            this.v1List.Size = new System.Drawing.Size(180, 436);
            this.v1List.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 503);
            this.Controls.Add(this.v1List);
            this.Controls.Add(this.retinaList);
            this.Controls.Add(this.retinaLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label retinaLabel;
        private System.Windows.Forms.ListBox retinaList;
        private System.Windows.Forms.ListBox v1List;
    }
}

