﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Image.Classes
{
    class Retina
    {
        public int filledAmount;
        public bool fire = false;

        public override string ToString()
        {
            string ret;
            if (fire)
            {
                ret = "On";
            }
            else
            {
                ret = "Off";
            }

            return ret;
        }
    }
}
